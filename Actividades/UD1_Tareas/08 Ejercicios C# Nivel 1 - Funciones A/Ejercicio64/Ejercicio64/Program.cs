﻿using System.Collections;

internal class Program {
    private static void Main(string[] args) {
        ArrayList listaNumeros = new ArrayList();
        Console.WriteLine("Introduce el primer numero: ");
        int numero = Convert.ToInt32(Console.ReadLine());
        listaNumeros.Add(numero); 
        Console.WriteLine("Introduce el segundo numero: ");
        numero = Convert.ToInt32(Console.ReadLine());
        listaNumeros.Add(numero);
        Console.WriteLine("Introduce el tercer numero: ");
        numero = Convert.ToInt32(Console.ReadLine());
        listaNumeros.Add(numero);
        Console.WriteLine("Introduce el cuarto numero: ");
        numero = Convert.ToInt32(Console.ReadLine());
        listaNumeros.Add(numero);
        Console.WriteLine("Introduce el quinto numero: ");
        numero = Convert.ToInt32(Console.ReadLine());
        listaNumeros.Add(numero);
        sumar(listaNumeros);
    }
    static void sumar(ArrayList listaNumeros) {
        int suma = 0;
        for (int i = 0; i < listaNumeros.Count; i++) {
            suma += Convert.ToInt32(listaNumeros[i]);
        }
        Console.WriteLine(suma);
    }
}