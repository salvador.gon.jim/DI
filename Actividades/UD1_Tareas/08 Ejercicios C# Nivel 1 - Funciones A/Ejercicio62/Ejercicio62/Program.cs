﻿using System.Diagnostics.Tracing;

internal class Program
{
    private static void Main(string[] args) {
        Console.WriteLine("Introduce un texto");
        string texto = Console.ReadLine();
        contarEspacios(texto);
    }
    static void contarEspacios(string textoIntroducido) {
        int numumeroEspacios = 0;
        for (int i = 0; i < textoIntroducido.Length; i++) {
            char caracter = textoIntroducido[i];
            if(caracter == ' ') {
                numumeroEspacios++;
            }
        }
        Console.WriteLine(numumeroEspacios);
    }
}