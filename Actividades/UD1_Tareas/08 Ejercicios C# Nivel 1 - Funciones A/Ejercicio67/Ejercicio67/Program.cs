﻿internal class Program {
    public static void Main(string[] args) {
        int numeroPrimero = 20;
        int numeroSegundo = 50;
        Swap(numeroPrimero, numeroSegundo);
    }
    public static void Swap(int numeroPrimero, int numeroSegundo) {
        int temp = numeroPrimero;
        numeroPrimero = numeroSegundo;
        numeroSegundo = temp;
    }
}