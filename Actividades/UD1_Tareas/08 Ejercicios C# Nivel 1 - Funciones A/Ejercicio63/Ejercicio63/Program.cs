﻿internal class Program {
    private static void Main(string[] args) {
        Console.WriteLine("Introduce un texto:");
        string textoIntroducido = Console.ReadLine();
        escribirCentradoSubrayado(textoIntroducido);
    }
    static void escribirCentradoSubrayado(string textoIntroducido) {
        for (int i = 0; i <= 80; i++) {
            Console.Write(" ");
        }
        Console.Write(textoIntroducido);
        Console.WriteLine();
        for (int i = 0;i <= 80; i++) {
            Console.Write(" ");
        }
        for (int i = 0; i < textoIntroducido.Length; i++) {
            Console.Write("-");
        }
    }
}