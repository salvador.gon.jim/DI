﻿internal class Program
{
    private static void Main(string[] args) {
        saludar("Juan");
        despedirse();
    }
    static void saludar(String nombre) {
        Console.WriteLine("Hola " + nombre);
    }
    static void despedirse() {
        Console.WriteLine("¡Adios!");
    }
}