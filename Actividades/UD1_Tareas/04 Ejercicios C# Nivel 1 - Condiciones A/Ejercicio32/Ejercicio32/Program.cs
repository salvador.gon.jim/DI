﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un numero: ");
        int numero = Convert.ToInt32(Console.ReadLine());
        while (numero != 0) {
            numero = numero * 10;
            Console.WriteLine(numero);
            Console.WriteLine("Introduce un numero: ");
            numero = Convert.ToInt32(Console.ReadLine());
        }
    }
}