﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce el rango menor: ");
        int rangoMenor = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Introduce el rango mayor: ");
        int rangoMayor = Convert.ToInt32(Console.ReadLine());
        for(int i = rangoMenor; i <= rangoMayor; i++) {
            Console.WriteLine($"{i} ");
        }
    }
}