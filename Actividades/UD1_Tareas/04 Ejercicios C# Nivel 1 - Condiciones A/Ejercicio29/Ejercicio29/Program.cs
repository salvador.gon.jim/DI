﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce el primer numero: ");
        int numeroPrimero = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Introduce la operacion +(Suma), -(Resta), *(Multiplicacion) y /(Division): ");
        String simbolo = Console.ReadLine();
        Console.WriteLine("Introduce el segundo numero: ");
        int numeroSegundo = Convert.ToInt32(Console.ReadLine());
        int resultado;
        switch(simbolo){
            case "+":
                resultado = numeroPrimero + numeroSegundo;
                Console.WriteLine($"{numeroPrimero} {simbolo} {numeroSegundo} = {resultado}");
                break;
            case "-":
                resultado = numeroPrimero - numeroSegundo;
                Console.WriteLine($"{numeroPrimero} {simbolo} {numeroSegundo} = {resultado}");
                break;
            case "*":
                resultado = numeroPrimero * numeroSegundo;
                Console.WriteLine($"{numeroPrimero} {simbolo} {numeroSegundo} = {resultado}");
                break;
            case "/":
                resultado = numeroPrimero / numeroSegundo;
                Console.WriteLine($"{numeroPrimero} {simbolo} {numeroSegundo} = {resultado}");
                break;
            default:
                Console.WriteLine("Caracter no reconocido");
                break;
        }
    }
}