﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un numero: ");
        int numero = Convert.ToInt32(Console.ReadLine());
        if (numero > 0){
            Console.WriteLine("Positivo");
        } else if ( numero < 0){
            Console.WriteLine("Negativo");
        } else{
            Console.WriteLine("Neutro");
        }
    }
}