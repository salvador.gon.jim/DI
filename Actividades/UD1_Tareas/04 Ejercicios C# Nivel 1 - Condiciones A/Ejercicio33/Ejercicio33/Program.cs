﻿internal class Program
{
    private static void Main(string[] args)
    {
        int numero;
        do {
            Console.WriteLine("Introduce un numero: ");
            numero = Convert.ToInt32(Console.ReadLine());
            numero = numero * 10;
            if (numero != 0) {
                Console.WriteLine(numero);
            }
        } while (numero != 0);
    }
}