﻿internal class Program
{
    private static void Main(string[] args)
    {
        int numero;
        int suma = 0;
        do {
            Console.WriteLine("Introduce un numero: ");
            numero = Convert.ToInt32(Console.ReadLine());
            suma = suma + numero;
            if (numero != 0) {
                Console.WriteLine(suma);
            }
        } while (numero != 0);
        Console.WriteLine("Terminado");
    }
}