﻿internal class Program
{
    private static void Main(string[] args)
    {
        int[] listaNumeros = new int[5];
        int suma = 0;
        int media;
        int numeroMayor = int.MinValue;
        int numeroMenor = int.MaxValue;
        for (int i = 0; i < listaNumeros.Length; i++) {
            Console.WriteLine("Introduce un numero: ");
            listaNumeros[i] = Convert.ToInt32(Console.ReadLine());
            suma += listaNumeros[i];
        }
        for (int i = 0; i < listaNumeros.Length; i++) {
            if (listaNumeros[i] > numeroMayor) {
                numeroMayor = listaNumeros[i];
            }
            if (listaNumeros[i] < numeroMenor) {
                numeroMenor = listaNumeros[i];
            }
        }
        media = suma / listaNumeros.Length;
        Console.WriteLine($"Suma: {suma}");
        Console.WriteLine($"Media: {media}");
        Console.WriteLine($"Numero mayor: {numeroMayor}");
        Console.WriteLine($"Numero menor: {numeroMenor}");
    }
}