﻿internal class Program {
    public static void Main() {
        try {
            string runtaFichero = @"input.txt";
            using (StreamReader streamReader = File.OpenText(runtaFichero)) {
                string contenido;
                while ((contenido = streamReader.ReadLine()) != null) {
                    Console.WriteLine(contenido);
                }
            }
        } catch (Exception e) {
            Console.WriteLine("El fichero no se puede leer:");
            Console.WriteLine(e.Message);
        }
    }
}