﻿using System.Collections;

internal class Program
{
    public static void Main()
    {
        try
        {
            ArrayList listaPalabras = new ArrayList();
            string runtaFichero = @"input.txt";
            using (StreamReader streamReader = File.OpenText(runtaFichero))
            {
                string palabra;
                while ((palabra = streamReader.ReadLine()) != null)
                {
                    listaPalabras.Add(palabra);
                }
            }
            for (int i = 0; i < listaPalabras.Count; i++)
            {
                Console.WriteLine(listaPalabras[i]);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("El fichero no se puede leer:");
            Console.WriteLine(e.Message);
        }
    }
}