﻿using System.Collections;
internal class Program {
    public static void Main() {
        try {
            ArrayList listaPalabras = new ArrayList();
            string palabra;
            Console.WriteLine("Escribe la primera palabra: ");
            palabra = Console.ReadLine();
            listaPalabras.Add(palabra);
            Console.WriteLine("Escribe la segunda palabra: ");
            palabra = Console.ReadLine();
            listaPalabras.Add(palabra);
            Console.WriteLine("Escribe la tercera palabra: ");
            palabra = Console.ReadLine();
            listaPalabras.Add(palabra);
            string rutaFichero = @"out.txt";
            using (StreamWriter streamWriter = File.CreateText(rutaFichero)) {
                for (int i = 0; i < listaPalabras.Count; i++) {
                    streamWriter.WriteLine(listaPalabras[i]);
                }
            }
        }
        catch (Exception e) {
            Console.WriteLine("El fichero no se puede leer:");
            Console.WriteLine(e.Message);
        }
    }
}