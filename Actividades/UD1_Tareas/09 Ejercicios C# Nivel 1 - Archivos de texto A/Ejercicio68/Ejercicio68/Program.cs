﻿internal class Program {
    public static void Main() {
        try {
            StreamReader streamReader = new StreamReader("input.txt");
                string linea;
                while ((linea = streamReader.ReadLine()) != null) {
                    Console.WriteLine(linea);
            }
        } catch (Exception e) {
            Console.WriteLine("El fichero no se puede leer:");
            Console.WriteLine(e.Message);
        }
    }
}