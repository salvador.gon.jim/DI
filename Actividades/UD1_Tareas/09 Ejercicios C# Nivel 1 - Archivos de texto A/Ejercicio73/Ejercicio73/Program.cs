﻿using System.Collections;
internal class Program {
    public static void Main() {
        try {
            string palabra;
            ArrayList listaPalabras = new ArrayList();
            do {
                Console.WriteLine("Escribe palabras: ");
                palabra = Console.ReadLine();
                listaPalabras.Add(palabra);
            } while (palabra != "");
            string rutaFichero = @"out.txt";
            using (StreamWriter streamWriter = File.AppendText(rutaFichero)) {
                for (int i = 0; i < listaPalabras.Count; i++) {
                    streamWriter.WriteLine(listaPalabras[i]);
                }
            }
        }
        catch (Exception e) {
            Console.WriteLine("El fichero no se puede leer:");
            Console.WriteLine(e.Message);
        }
    }
}