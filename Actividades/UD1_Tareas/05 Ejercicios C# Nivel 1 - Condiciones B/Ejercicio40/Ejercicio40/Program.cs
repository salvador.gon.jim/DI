﻿internal class Program
{
    private static void Main(string[] args)
    {
        try {
            int divisor;
            int dividendo;
            do {
                Console.WriteLine("Introduce el divisor: ");
                divisor = Convert.ToInt32(Console.ReadLine());
                if (divisor != 0){
                    Console.WriteLine("Introduce el dividendo: ");
                    dividendo = Convert.ToInt32(Console.ReadLine());
                    int resultadoDivision = divisor / dividendo;
                    Console.WriteLine($"La division es {resultadoDivision}");
                    int restoDivision = divisor % dividendo;
                    Console.WriteLine($"El resto es {restoDivision}");
                }
            } while (divisor != 0);
            Console.WriteLine("Adios");
        } catch (Exception e) {
            Console.WriteLine("No se puede dividir entre 0");
            Main(args);
        }
    }
}