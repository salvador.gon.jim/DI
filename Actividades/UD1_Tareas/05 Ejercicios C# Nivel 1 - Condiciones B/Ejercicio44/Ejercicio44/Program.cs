﻿internal class Program
{
    private static void Main(string[] args)
    {
        try {
            String numeroString = "";
            do {
                Console.WriteLine("Introduce un numero: ");
                int numero = Convert.ToInt32(Console.ReadLine());
                numeroString = "" + numero;
                if (numeroString.Contains("-")) {
                    Console.WriteLine("(Advertencia: es un numero negativo)");
                    Console.WriteLine(numeroString.Length - 1 + " dígitos");
                } else {
                    Console.WriteLine(numeroString.Length + " dígitos.");
                }
            } while (!numeroString.Contains("-"));
        } catch {
            Console.WriteLine("No se ha introducido un numero entero");
        }
    }
}