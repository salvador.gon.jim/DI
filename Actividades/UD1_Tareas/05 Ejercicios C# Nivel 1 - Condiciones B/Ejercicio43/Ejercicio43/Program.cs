﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce el rango menor: ");
        int numeroMenor = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Introduce el rango mayor: ");
        int numeroMayor = Convert.ToInt32(Console.ReadLine());
        int rango = numeroMayor - numeroMenor;
        for (int i = numeroMenor; i <= numeroMayor; i++) {
            Console.Write(i + " ");
        }
        Console.WriteLine();
        int contador = numeroMenor;
        do {

            Console.Write(contador + " ");
            contador++;

        } while (contador <= numeroMayor);
        Console.WriteLine();
        contador = numeroMenor;
        while (contador <= numeroMayor) {
            Console.Write(contador + " ");
            contador++;
        }
    }
}