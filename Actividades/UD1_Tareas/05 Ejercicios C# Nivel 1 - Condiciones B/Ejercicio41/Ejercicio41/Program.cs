﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce el rango menor: ");
        int numeroMenor = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Introduce el rango mayor: ");
        int numeroMayor = Convert.ToInt32(Console.ReadLine());
        int rango = numeroMayor - numeroMenor;
        for (int i = 0; i <= rango; i++) {
            for (int j = 1; j <= 10; j++) {
                int resultado = numeroMenor * j;
                Console.WriteLine($"{numeroMenor}x{j}= {resultado}");
            }
            Console.WriteLine();
            numeroMenor++;
        }
    }
}