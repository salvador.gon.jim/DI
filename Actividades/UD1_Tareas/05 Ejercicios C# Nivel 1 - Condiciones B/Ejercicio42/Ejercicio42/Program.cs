﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce el la nota: ");
        int nota = Convert.ToInt32(Console.ReadLine());
       switch (nota)
        {
            case 10:
                Console.WriteLine("Matricula de honor");
                break;
                case 9:
                Console.WriteLine("Sobresaliente");
                break;
            case 8:
            case 7:
                Console.WriteLine("Notable");
                break;
                case 6:
                Console.WriteLine("Bien");
                break;
            case 5:
                Console.WriteLine("Aprobado");
                break;
                case 4:
                case 3:
                case 2:
                case 1:
                case 0:
                Console.WriteLine("Suspenso");
                break;
                default:
                Console.WriteLine("La nota debe ser entre 0 y 10");
                break;

        }
    }
}