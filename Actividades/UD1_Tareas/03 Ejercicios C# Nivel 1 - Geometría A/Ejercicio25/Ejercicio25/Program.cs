﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introdice un ancho: ");
        int baseTriangulo = Convert.ToInt32(Console.ReadLine());
        for (int i = 0; i < baseTriangulo; i++)
        {
            for (int k = 0; k < i; k++)
            {
                Console.Write(" ");
            }
            for (int j = 0; j < baseTriangulo - i; j++)
            {
                Console.Write("*");
            }
            Console.WriteLine();
        }
    }
}