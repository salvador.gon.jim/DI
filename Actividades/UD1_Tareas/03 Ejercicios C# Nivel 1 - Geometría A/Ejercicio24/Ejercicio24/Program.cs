﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introdice un numero o simbolo: ");
        int simbolo = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Introdice un ancho: ");
        int baseTriangulo = Convert.ToInt32(Console.ReadLine());
        for (int i = 0; i < baseTriangulo; i++)
        {
            for (int j = 0; j < baseTriangulo - i; j++)
            {
                Console.Write(simbolo);
            }
            for (int k = 0; k < i; k++)
            {
                Console.Write(" ");
            }
            Console.WriteLine();
        }
    }
}