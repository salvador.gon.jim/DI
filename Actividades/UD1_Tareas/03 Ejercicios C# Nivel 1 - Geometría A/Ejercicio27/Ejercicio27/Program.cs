﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introdice el rango menor: ");
        int rangoMenor = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Introdice el rango mayor: ");
        int rangoMayor = Convert.ToInt32(Console.ReadLine());
        int y;
        for (int x = rangoMenor; x <= rangoMayor; x++)
        {
            y = (x - 4) * (x - 4);
            for (int j = 0; j < y; j++)
            {
                Console.Write("*");
            }
            Console.WriteLine();
        }
    }
}