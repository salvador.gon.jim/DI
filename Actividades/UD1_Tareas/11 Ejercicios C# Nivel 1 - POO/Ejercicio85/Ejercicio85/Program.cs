﻿using Ejercicio85;

internal class Program {
    private static void Main(string[] args) {
        Persona persona;
        List<Persona> listaPersonas = new List<Persona>();
        for (int i = 0; i < 3; i++) {
            Console.WriteLine("Introduce el nombre:");
            string nombrePersona = Console.ReadLine();
            persona = new Persona(nombrePersona);
            listaPersonas.Add(persona);
        }
        for (int i = 0; i < listaPersonas.Count; i++) {
            Console.WriteLine(listaPersonas[i]);
        }
    }
}