﻿namespace Ejercicio85 {
    internal class Persona {
        private string nombre;
        public Persona(string nombre) {
            this.nombre = nombre;
        }
        public override string ToString() {
            return "¡Hola! Mi nombre es " + nombre;
        }
    }
}