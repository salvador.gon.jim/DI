﻿using Ejercicio86;
internal class Program{
    private static void Main(string[] args) {
        int numPersonas = 3;
        Persona[] listaPersonas = new Persona[numPersonas];
        for (int i = 0; i < numPersonas; i++) {
            Console.WriteLine("Introduce el nombre:");
            string nombrePersona = Console.ReadLine();
            listaPersonas[i] = new Persona(nombrePersona);
        }
        for (int i = 0; i < numPersonas; i++) {
            Console.WriteLine(listaPersonas[i]);
        }
    }
}