﻿namespace Ejercicio86 {
    internal class Persona {
        private string nombre;
        public Persona(string nombre) {
            this.nombre = nombre;
        }
        ~Persona() {
            this.nombre = string.Empty;
        }
        public override string ToString() {
            return "¡Hola! Mi nombre es " + nombre;
        }
    }
}
