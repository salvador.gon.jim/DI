﻿namespace Ejercicio89
{
    internal class Persona
    {
        protected string nombre;
        public Persona() { }
        public Persona(string nombre)
        {
            this.nombre = nombre;
        }
        public override string ToString()
        {
            return "¡Hola! Mi nombre es " + nombre;
        }
    }
}
