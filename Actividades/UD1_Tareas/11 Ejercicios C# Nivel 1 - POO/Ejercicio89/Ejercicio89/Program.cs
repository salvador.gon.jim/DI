﻿using Ejercicio89;

internal class Program
{
    private static void Main(string[] args)
    {
        int totalPersonas = 3;
        Persona[] listaPersonas = new Persona[totalPersonas];
        Console.WriteLine("Introduce los nombres: ");
        string nombre = Console.ReadLine();
        listaPersonas[0] = new Profesor(nombre);
        nombre = Console.ReadLine();
        listaPersonas[1] = new Estudiante(nombre);
        nombre = Console.ReadLine();
        listaPersonas[2] = new Estudiante(nombre);
        for (int i = 0; i < totalPersonas; i++)
        {
            if (i == 0)
            {
                ((Profesor)listaPersonas[i]).Explicar();
            }
            else
            {
                ((Estudiante)listaPersonas[i]).Estudiar();
            }
        }
    }
}