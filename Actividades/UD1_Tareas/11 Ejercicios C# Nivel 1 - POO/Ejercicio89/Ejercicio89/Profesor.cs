﻿namespace Ejercicio89
{
    internal class Profesor : Persona
    {
        public Profesor(string nombre) : base(nombre)
        {
            this.nombre = nombre;
        }
        public void Explicar()
        {
            Console.WriteLine("Estoy explicando");
        }
    }
}
