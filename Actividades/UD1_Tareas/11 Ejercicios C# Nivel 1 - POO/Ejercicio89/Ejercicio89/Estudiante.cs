﻿namespace Ejercicio89
{
    internal class Estudiante : Persona
    {
        public Estudiante(string nombre) : base(nombre)
        {
            this.nombre = nombre;
        }
        public void Estudiar()
        {
            Console.WriteLine("Estoy estudiando.");
        }
    }
}