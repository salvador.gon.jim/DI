﻿namespace Ejercicio92
{
    internal abstract class Animal
    {
        private string nombre;
        public void SetNombre(string nombre)
        {
            nombre = nombre;
        }
        public string GetNombre()
        {
            return nombre;
        }
        public abstract void Comer();
    }
}