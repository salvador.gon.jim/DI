﻿using Ejercicio92;

internal class Program
{
    private static void Main(string[] args)
    {
        Perro perro = new Perro();
        Console.WriteLine("Introduce el nombre del perro:");
        string nombrePerro = Console.ReadLine();
        perro.SetNombre(nombrePerro);
        Console.WriteLine(nombrePerro);
        perro.Comer();
    }
}