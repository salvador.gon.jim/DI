﻿namespace Ejercicio91
{
    internal interface IVehiculo
    {
        void Conducir();
        bool Repostar(int cantidad);
    }
}
