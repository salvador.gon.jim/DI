﻿namespace Ejercicio91
{
    internal class Coche : IVehiculo
    {
        public int nivelCombustible;
        public Coche(int cantidadCombustible)
        {
            nivelCombustible = cantidadCombustible;
        }
        public void Conducir()
        {
            if (nivelCombustible > 0)
            {
                Console.WriteLine("Conduciendo");
            } else {
                Console.WriteLine("Sin combustible");
            }
        }
        public bool Repostar(int cantidad)
        {
            nivelCombustible += cantidad;
            return true;
        }
    }
}
