﻿using Ejercicio91;

internal class Program
{
    private static void Main(string[] args)
    {
        Coche coche = new Coche(0);
        coche.Conducir();
        Console.WriteLine("Introduce la cantidad a repostar: ");
        int gasolina = Convert.ToInt32(Console.ReadLine());
        coche.Repostar(gasolina);
        coche.Conducir();
    }
}