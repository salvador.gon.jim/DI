﻿namespace Ejercicio88
{
    internal class AlbumFotosTest
    {
        public static void Main()
        {
            AlbumFotos miAlbum1 = new AlbumFotos();
            Console.WriteLine(miAlbum1.GetNumeroPaginas());

            AlbumFotos miAlbum2 = new AlbumFotos(24);
            Console.WriteLine(miAlbum2.GetNumeroPaginas());

            SuperAlbumFotos miGranAlbum1 = new SuperAlbumFotos();
            Console.WriteLine(miGranAlbum1.GetNumeroPaginas());
        }
    }
}
