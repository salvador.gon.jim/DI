﻿namespace Ejercicio88
{
    internal class AlbumFotos
    {
        protected int numPaginas;
        public AlbumFotos()
        {
            numPaginas = 16;
        }
        public AlbumFotos(int numPaginas)
        {
            this.numPaginas = numPaginas;
        }
        public int GetNumeroPaginas()
        {
            return numPaginas;
        }
    }
}
