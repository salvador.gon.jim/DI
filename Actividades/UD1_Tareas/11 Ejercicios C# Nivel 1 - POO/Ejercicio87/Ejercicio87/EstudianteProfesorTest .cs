﻿namespace Ejercicio87
{
    internal class EstudianteProfesorTest
    {
        public static void Main()
        {
            Persona persona = new Persona();
            persona.Saludar();

            Estudiante estudiante = new Estudiante(21);
            estudiante.Saludar();
            estudiante.VerEdad();
            estudiante.Estudiar();

            Profesor profesor = new Profesor();
            profesor.Saludar();
            profesor.Explicar();
        }
    }
}
