﻿namespace Ejercicio87
{
    internal class Persona
    {
        protected int edad;
        public Persona() { }
        public Persona(int edad)
        {
            this.edad = edad;
        }
        public void Saludar()
        {
            Console.WriteLine("Hola");
        }
        public void SetEdad(int edad)
        {
            this.edad = edad;
        }
    }
}
