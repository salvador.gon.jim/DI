﻿namespace Ejercicio87
{
    internal class Estudiante : Persona
    {
        public Estudiante() { }
        public Estudiante(int edad) : base(edad)
        {
        }
        public void Estudiar()
        {
            Console.WriteLine("Estoy estudiando.");
        }
        public void VerEdad()
        {
            Console.WriteLine("Mi edad es " + this.edad);
        }
    }
}