﻿internal class Program {
    private static void Main(string[] args) {
        int valoresTotales = 0;
        int maximosValoresPermitidos = 1000;
        int[] listaNumeros = new int[maximosValoresPermitidos];
        Random random = new Random();
        int contador = 0;
        int maxValor;
        int minValor;
        int suma;
        double media;
        int opcion;
        do {
            Console.WriteLine("1. Añadir");
            Console.WriteLine("2. Ver");
            Console.WriteLine("3. Buscar");
            Console.WriteLine("4. Estadísticas");
            Console.WriteLine("5. Salir");
            Console.WriteLine("Introduzca una opción (1-5): ");
            opcion = Convert.ToInt32(Console.ReadLine());
            switch (opcion) {
                case 1: // Opción para añadir valores
                    Console.WriteLine("Ingresa el número de valores a agregar: ");
                    int nuevosValores = Convert.ToInt32(Console.ReadLine());
                    if (valoresTotales + nuevosValores <= maximosValoresPermitidos) {
                        for (int i = 0; i < nuevosValores; i++) {
                            listaNumeros[valoresTotales + i] = random.Next(1, 1000);
                            contador++;
                        }
                        valoresTotales += nuevosValores;
                    } else {
                        Console.WriteLine("Has superado el limite de valores permitidos.");
                    }
                    break;
                case 2: // Ver
                    if (valoresTotales > 0) {
                        for (int i = 0; i < listaNumeros.Length; i++) {
                            if (listaNumeros[i] > 0) {
                                Console.Write(listaNumeros[i] + " ");
                            }  
                        }
                        Console.WriteLine();
                    } else {
                        Console.WriteLine("Datos no disponibles");
                    }
                    break;
                case 3: // Buscar
                    Console.WriteLine("Introduce un número a buscar:");
                    int numeroABuscar = Convert.ToInt32(Console.ReadLine());
                    if (valoresTotales > 0) {
                        if (listaNumeros.Contains(numeroABuscar)) {
                            Console.WriteLine($"El número {numeroABuscar} existe");
                        } else {
                            Console.WriteLine($"El número {numeroABuscar} no existe");
                        }
                    } else {
                        Console.WriteLine("Datos no disponibles");
                    }
                    break;
                case 4: //Opcion estadisticas
                    if (valoresTotales > 0) {
                        maxValor = listaNumeros.Max();
                        Console.WriteLine("Total datos: " + contador);
                        suma = listaNumeros.Sum();
                        Console.WriteLine("Suma: " + suma);
                        media = listaNumeros.Average();
                        Console.WriteLine("Media: " + media);
                        minValor = listaNumeros.Min();
                        Console.WriteLine("Valor minimo: " + minValor);
                        maxValor = listaNumeros.Max();
                        Console.WriteLine("Valor maximo: " + maxValor);
                    } else {
                        Console.WriteLine("Datos no disponibles");
                    }
                    break;
                case 5:
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Opciones validas (1-5)");
                    break;
            }
        }
        while (opcion != 5);
    }
}