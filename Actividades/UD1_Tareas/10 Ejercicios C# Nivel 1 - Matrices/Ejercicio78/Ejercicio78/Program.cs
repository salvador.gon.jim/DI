﻿internal class Program { 
    static void Main(string[] args)
    {
        Console.WriteLine("Introduce una palabra: ");
        string textoEntrada = Console.ReadLine();

        byte contador = 1;
        char caracter;
        int[] codigosAscii = new int[textoEntrada.Length];

        // Convierte el texto a códigos ASCII
        for (int i = 0; i < textoEntrada.Length; i++)
        {
            caracter = Convert.ToChar(textoEntrada.Substring(i, contador));
            codigosAscii[i] = Convert.ToInt32(caracter);
        }

        int tamañoLetra = 7,
            asciiInicial = 32,

            contadorLinea = 0,
            contadorLetra = 0,
            contadorCaracter = 0;

        bool letraEncontrada = false;
        string[] resultado = new string[tamañoLetra];

        for (int i = 0; i < codigosAscii.Length; i++)
        {
            for (int fila = 0; fila < letras.Length; fila++)
            {
                if (contadorLetra == 8)
                {
                    fila += tamañoLetra - 1;
                    contadorLetra = 0;
                    contadorCaracter = 0;
                }

                while ((!letraEncontrada) && (contadorLetra < 8))
                {
                    if (codigosAscii[i] == asciiInicial)
                        letraEncontrada = true;
                    else
                    {
                        asciiInicial++;
                        contadorCaracter += tamañoLetra;
                        contadorLetra++;
                    }
                }

                if ((letraEncontrada) && (contadorLinea < 7))
                {
                    if (i > 0)
                    {
                        resultado[contadorLinea] = resultado[contadorLinea] + letras[fila].Substring(contadorCaracter, tamañoLetra);
                    }
                    else
                    {
                        resultado[contadorLinea] = letras[fila].Substring(contadorCaracter, tamañoLetra);
                    }
                    contadorLinea++;
                }
            }

            contadorCaracter = 0;
            contadorLinea = 0;
            contadorLetra = 0;
            asciiInicial = 32;
            letraEncontrada = false;
        }

        for (int i = 0; i < resultado.Length; i++)
        {
            Console.WriteLine(resultado[i]);
        }
    }

    static string[] letras =  {
        "         ###  ### ###  # #   ##### ###   #  ##     ###  ",
        "         ###  ### ###  # #  #  #  ## #  #  #  #    ###  ",
        "         ###   #   # ########  #   ### #    ##      #   ",
        "          #            # #   #####    #    ###     #    ",
        "                     #######   #  #  # ####   # #       ",
        "         ###           # #  #  #  # #  # ##    #        ",
        "         ###           # #   ##### #   ### #### #       ",
        "   ##    ##                                            #",
        "  #        #   #   #    #                             # ",
        " #          #   # #     #                            #  ",
        " #          # ####### #####   ###   #####           #   ",
        " #          #   # #     #     ###           ###    #    ",
        "  #        #   #   #    #      #            ###   #     ",
        "   ##    ##                   #             ###  #      ",
        "  ###     #    #####  ##### #      ####### ##### #######",
        " #   #   ##   #     ##     ##    # #      #     ##    # ",
        "#     # # #         #      ##    # #      #          #  ",
        "#     #   #    #####  ##### #    # ###### ######    #   ",
        "#     #   #   #            ########      ##     #  #    ",
        " #   #    #   #      #     #     # #     ##     #  #    ",
        "  ###   ##### ####### #####      #  #####  #####   #    ",
        " #####  #####    #     ###      #           #     ##### ",
        "#     ##     #  ###    ###     #             #   #     #",
        "#     ##     #   #            #     #####     #        #",
        " #####  ######         ###   #                 #     ## ",
        "#     #      #   #     ###    #     #####     #     #   ",
        "#     ##     #  ###     #      #             #          ",
        " #####  #####    #     #        #           #       #   ",
        " #####    #   ######  ##### ###### ############## ##### ",
        "#     #  # #  #     ##     ##     ##      #      #     #",
        "# ### # #   # #     ##      #     ##      #      #      ",
        "# # # ##     ####### #      #     ######  #####  #  ####",
        "# #### ########     ##      #     ##      #      #     #",
        "#      #     ##     ##     ##     ##      #      #     #",
        " ##### #     #######  ##### ###### ########       ##### ",
        "#     #  ###        ##    # #      #     ##     ########",
        "#     #   #         ##   #  #      ##   ####    ##     #",
        "#     #   #         ##  #   #      # # # ## #   ##     #",
        "#######   #         ####    #      #  #  ##  #  ##     #",
        "#     #   #   #     ##  #   #      #     ##   # ##     #",
        "#     #   #   #     ##   #  #      #     ##    ###     #",
        "#     #  ###   ##### #    # ########     ##     ########",
        "######  ##### ######  ##### ########     ##     ##     #",
        "#     ##     ##     ##     #   #   #     ##     ##  #  #",
        "#     ##     ##     ##         #   #     ##     ##  #  #",
        "###### #     #######  #####    #   #     ##     ##  #  #",
        "#      #   # ##   #        #   #   #     # #   # #  #  #",
        "#      #    # #    # #     #   #   #     #  # #  #  #  #",
        "#       #### ##     # #####    #    #####    #    ## ## ",
        "#     ##     ######## ##### #       #####    #          ",
        " #   #  #   #      #  #      #          #   # #         ",
        "  # #    # #      #   #       #         #  #   #        ",
        "   #      #      #    #        #        #               ",
        "  # #     #     #     #         #       #               ",
        " #   #    #    #      #          #      #               ",
        "#     #   #   ####### #####       # #####        #######",
        "  ###                                                   ",
        "  ###     ##   #####   ####  #####  ###### ######  #### ",
        "   #     #  #  #    # #    # #    # #      #      #    #",
        "        ###### #    # #      #    # #      #      #  ###",
        "    #   #    # #####  #      #    # #####  #####  #     ",
        "        #    # #    # #    # #    # #      #      #    #",
        "        #    # #####   ####  #####  ###### #       #### ",
        "                                                        ",
        " #    #    #        # #    # #      #    # #    #  #### ",
        " #    #    #        # #   #  #      ##  ## ##   # #    #",
        " ######    #        # ####   #      # ## # # #  # #    #",
        " #    #    #        # #  #   #      #    # #  # # #    #",
        " #    #    #   #    # #   #  #      #    # #   ## #    #",
        " #    #    #    ####  #    # ###### #    # #    #  #### ",
        "                                                        ",
        " #####   ####  #####   ####   ##### #    # #    # #    #",
        " #    # #    # #    # #         #   #    # #    # #    #",
        " #    # #    # #    #  ####     #   #    # #    # #    #",
        " #####  #  # # #####       #    #   #    # #    # # ## #",
        " #      #   #  #   #  #    #    #   #    #  #  #  ##  ##",
        " #       ### # #    #  ####     #    ####    ##   #    #",
        "                       ###     #     ###   ##    # # # #",
        " #    #  #   # ###### #        #        # #  #  # # # # ",
        "  #  #    # #      #  #        #        #     ## # # # #",
        "   ##      #      #  ##                 ##        # # # ",
        "   ##      #     #    #        #        #        # # # #",
        "  #  #     #    #     #        #        #         # # # ",
        " #    #    #   ######  ###     #     ###         # # # #"
        };

}