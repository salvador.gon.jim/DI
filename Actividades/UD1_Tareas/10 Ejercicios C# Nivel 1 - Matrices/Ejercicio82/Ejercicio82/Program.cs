﻿internal class Program {
    struct Coche {
        public string marcaCoche;
        public int añoFabricacion;
    }
    static void Main(string[] args) {
        Coche aux;
        int numCoches = 3;
        Coche[] listaCoches = new Coche[numCoches];
        for (int i = 0; i < numCoches; i++) {
            listaCoches[i] = new Coche()
            {
                marcaCoche = Console.ReadLine(),
                añoFabricacion = Convert.ToInt32(Console.ReadLine())
            };
        }
        //Ordenacion burbuja
        for (int i = 0; i < listaCoches.Length; i++) {
            for (int j = 0; j < listaCoches.Length - 1; j++)
            {
                if (listaCoches[j].añoFabricacion > listaCoches[j + 1].añoFabricacion)
                {
                    aux = listaCoches[j];
                    listaCoches[j] = listaCoches[j + 1];
                    listaCoches[j + 1] = aux;
                }
            }
        }
        //Imprimir
        for (int i = 0; i < numCoches; i++) {
            Console.WriteLine($"Marca: {listaCoches[i].marcaCoche},  Año de fabricacion: {listaCoches[i].añoFabricacion}");
        }
    }
}