﻿internal class Program
{
    private static void Main(string[] args){
        int cantidadNumeros = 10;
        int[] listaNumeros = new int[cantidadNumeros];
        Console.WriteLine($"Introduce {cantidadNumeros} numeros: ");
        for (int i = 0; i  < listaNumeros.Length; i++) {
            listaNumeros[i] = Convert.ToInt32(Console.ReadLine());
        }
        for (int j = 0; j < cantidadNumeros; j++) {
            if (listaNumeros[j] % 2 == 0) {
                Console.Write(listaNumeros[j] + " ");
            }
        }
    }
}