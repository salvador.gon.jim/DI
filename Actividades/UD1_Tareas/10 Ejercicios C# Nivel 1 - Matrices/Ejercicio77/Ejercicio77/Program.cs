﻿internal class Program
{
    private static void Main(string[] args)
    {
        int cantidadNumeros = 10;
        int[] listaNumeros = new int[cantidadNumeros];
        int[] numerosPositivos = new int[cantidadNumeros];
        int[] numerosNegativos = new int[cantidadNumeros];
        int positivosEncontados = 0;
        int negativosEncontrados = 0;
        int mediaNegativos;
        int mediaPositivos;
        Console.WriteLine($"Introduce {cantidadNumeros} numeros: ");
        for (int i = 0; i < listaNumeros.Length; i++) {
            listaNumeros[i] = Convert.ToInt32(Console.ReadLine());
        }
        for (int i = 0; i < cantidadNumeros; i++) {
            if (listaNumeros[i] >= 0) {
                numerosPositivos[positivosEncontados] = listaNumeros[i];
                positivosEncontados++;
            } else {
                numerosNegativos[negativosEncontrados] = listaNumeros[i];
                negativosEncontrados++;
            }
        }
        mediaNegativos = numerosNegativos.Sum() / negativosEncontrados;
        mediaPositivos = numerosPositivos.Sum() / positivosEncontados;
        Console.WriteLine("La media de negativos suma: " + mediaNegativos);
        Console.WriteLine("La media de positivos suma: " + mediaPositivos);
    }
}