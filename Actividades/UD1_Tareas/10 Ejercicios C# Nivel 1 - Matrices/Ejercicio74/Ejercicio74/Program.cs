﻿internal class Program {
    private static void Main(string[] args) {
        int cantidadNumeros = 5;
        int[] listaNumeros = new int[cantidadNumeros];
        Console.WriteLine($"Introduce {cantidadNumeros} numeros: ");
        for (int i = 0; i < cantidadNumeros; i++) {
            listaNumeros[i] = Convert.ToInt32(Console.ReadLine());
        }
        foreach (int numero in listaNumeros.Reverse()) {
            Console.Write(numero + " ");
        }
    }
}