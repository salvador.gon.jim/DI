﻿class Program {
    static void Main(string[] args) {
        int numGrupo = 2;
        int numAlumnos = 5;
        double notasGrupo1 = 0;
        double notasGrupo2 = 0;
        double mediaGrupo1;
        double mediaGrupo2;
        double[,] notas = new double[numGrupo, numAlumnos];
        //Notas del grupo 1
        Console.WriteLine($"Ingrese las notas del primer grupo de {numAlumnos} alumnos:");
        for (int i = 0; i < numAlumnos; i++) {
            notas[0, i] = Convert.ToInt32(Console.ReadLine());
        }
        //Suma grupo 1
        for (int i = 0; i < numAlumnos; i++) {
            notasGrupo1 += notas[0, i];
        }
        //Media grupo 1
        mediaGrupo1 = notasGrupo1 / numAlumnos;
        //Notas del grupo 2
        Console.WriteLine($"Ingrese las notas del segundo grupo de {numAlumnos} alumnos:");
        for (int i = 0; i < numAlumnos; i++) {
            notas[1, i] = Convert.ToInt32(Console.ReadLine());
        }
        //Suma grupo 2
        for (int i = 0; i < numAlumnos; i++) {
            notasGrupo2 += notas[1, i];
        }
        //Media grupo 2
        mediaGrupo2 = notasGrupo2 / numAlumnos;
        //Imprimir
        Console.WriteLine($"La media del primer grupo de alumnos es: {mediaGrupo1}");
        Console.WriteLine($"La media del segundo grupo de alumnos es: {mediaGrupo2}");
    }
}
