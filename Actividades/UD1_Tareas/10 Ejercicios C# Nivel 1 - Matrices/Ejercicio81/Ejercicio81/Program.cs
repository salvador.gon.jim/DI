﻿internal class Program {
    private static void Main(string[] args) {
        int numFilas = 20;
        int numColumnas = 70;
        for (int i = 0; i < numFilas; i++) {
            for( int j = 0; j < numColumnas; j++) {
                Console.Write("X");
            }
            Console.WriteLine();
        }
    }
}