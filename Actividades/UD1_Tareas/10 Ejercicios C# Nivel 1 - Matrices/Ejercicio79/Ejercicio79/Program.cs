﻿internal class Program {
    private static void Main(string[] args) {
        int cantidadNumeros = 10;
        Console.WriteLine($"Introduce {cantidadNumeros} numeros: ");
        int[] listaNumeros = new int[cantidadNumeros];
        int aux;
        for (int i = 0; i < cantidadNumeros; i++) {
            listaNumeros[i] = Convert.ToInt32(Console.ReadLine());
        }

        for (int i = 0; i<listaNumeros.Length; i++) {
            for ( int j = 0; j<listaNumeros.Length - 1; j++) {
                if (listaNumeros[j] > listaNumeros[j + 1]) {
                    aux = listaNumeros[j];
                    listaNumeros[j] = listaNumeros[j + 1];
                    listaNumeros[j + 1] = aux;
                }
            }
        }
        for (int i = 0; i<listaNumeros.Length; i++) {
            Console.Write(listaNumeros[i] + " ");
        }
    }
}