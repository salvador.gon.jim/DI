﻿internal class Program {
    private static void Main(string[] args) {
        Console.WriteLine("Cuantos numeros quieres analizar?");
        int cantidadNumeros = Convert.ToInt32(Console.ReadLine());
        int numeroAComprobar = 5;
        int[] listaNumeros = new int[cantidadNumeros];
        int indiceMatriz = 0;
        string numero;
        Console.WriteLine($"Introduce numeros: ");
        do {
            numero = Console.ReadLine();
            if (numero != "fin") {
                listaNumeros[indiceMatriz] = Convert.ToInt32(numero);
                indiceMatriz++;
            }
        } while (numero != "fin");
        if (listaNumeros.Contains(numeroAComprobar)) {
            Console.WriteLine($"El numero {numeroAComprobar} existe.");
        } else {
            Console.WriteLine($"El numero {numeroAComprobar} no existe.");
        }
    }
}