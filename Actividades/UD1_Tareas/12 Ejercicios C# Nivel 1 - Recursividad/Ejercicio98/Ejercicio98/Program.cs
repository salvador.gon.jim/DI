﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduzca una cadena:");
        string cadena = Console.ReadLine();
        if(EsPalindromo(cadena))
        {
            Console.WriteLine(EsPalindromo(cadena));
        }
        else
        {
            Console.WriteLine("No es una palabra palindroma.");
        }
    }
    static bool EsPalindromo(string cadena)
    {
        bool esPalindromo = false;
        if (cadena.Length <= 1)
        {
            esPalindromo = true;
        }
        else
        {
            if (cadena[0] != cadena[cadena.Length - 1])
            {
                esPalindromo = false;
            }
            else
            {
                esPalindromo = EsPalindromo(cadena.Substring(1, cadena.Length - 2));
            }
        }
        return esPalindromo;
    }
}