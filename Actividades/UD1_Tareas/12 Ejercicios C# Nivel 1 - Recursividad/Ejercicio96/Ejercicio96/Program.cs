﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduzca un numero: ");
        int numero = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine(Factorial(numero));
    }
    static int Factorial(int numero)
    {
        int resultado;
        if (numero == 0)
        {
            resultado = 1;
        }
        else
        {
            resultado = numero * Factorial(numero - 1);
        }
        return resultado;
    }
}