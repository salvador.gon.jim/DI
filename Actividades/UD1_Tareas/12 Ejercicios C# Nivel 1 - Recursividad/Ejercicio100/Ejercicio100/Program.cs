﻿internal class Program
{
    private static void Main(string[] args)
    {
        string nombreFicheroEntrada = "input.txt";
        string nombreFicheroSalida = "output.txt";
        string palabrasFichero = File.ReadAllText(nombreFicheroEntrada);
        File.WriteAllText(nombreFicheroSalida, palabrasFichero.ToUpper());
    }
}