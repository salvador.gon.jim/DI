﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Ingresa el nombre del archivo: ");
        string nombreArchivo = Console.ReadLine();
        StreamReader archivo = File.OpenText(nombreArchivo);
        int contadorPalabras = 0;
        string linea;
        do
        {
            linea = archivo.ReadLine();
            if (linea != null)
            {
                contadorPalabras += linea.Split(" ").Length;
            }
        }
        while (linea != null);
    Console.WriteLine(contadorPalabras);
    }
}