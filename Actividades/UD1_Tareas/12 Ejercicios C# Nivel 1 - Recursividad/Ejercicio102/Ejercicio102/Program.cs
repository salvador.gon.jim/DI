﻿internal class Program
{
    private static void Main(string[] args)
    {
        string archivoEntrada = "input.txt";
        string archivoSalida = "output.inv";
        StreamReader streamReader = new StreamReader(archivoEntrada);
        StreamWriter streamWriter = new StreamWriter(archivoSalida);
        string contenido = streamReader.ReadToEnd();
        string contenidoInvertido = "";
        for (int i = contenido.Length - 1; i >= 0; i--)
        {
            contenidoInvertido += contenido[i];
        }
        streamWriter.Write(contenidoInvertido);
        streamReader.Close();
        streamWriter.Close();
    }
}