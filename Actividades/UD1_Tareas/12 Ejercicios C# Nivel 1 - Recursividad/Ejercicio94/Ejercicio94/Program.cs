﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduzca el primer numero:");
        int primerNumero = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Introduzca el segundo numero:");
        int segundoNumero = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine(Multiplicar(primerNumero, segundoNumero));
    }
    static int Multiplicar(int primerNumero, int segundoNumero)
    {
        int resultado;
        if (primerNumero == 0 || segundoNumero == 0)
        {
            resultado = 0;
        }
        else
        {
            resultado = primerNumero + Multiplicar(primerNumero, segundoNumero - 1);
        }
        return resultado;
    }
}