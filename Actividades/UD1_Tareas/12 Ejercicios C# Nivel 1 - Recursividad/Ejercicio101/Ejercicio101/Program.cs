﻿internal class Program
{
    private static void Main(string[] args)
    {
        string archivoEntrada = "input.txt";
        string archivoSalida = "output.txt";
        string contenido = File.ReadAllText(archivoEntrada);
        string contenidoCifrado = "";
        for (int i = 0; i < contenido.Length; i++)
        {
            char cifrado = (char)(contenido[i] + 13);
            contenidoCifrado += cifrado;
        }
        File.WriteAllText(archivoSalida, contenidoCifrado);
    }
}