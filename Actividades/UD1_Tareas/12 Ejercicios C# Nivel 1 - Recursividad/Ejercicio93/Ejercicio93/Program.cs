﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce la base: ");
        int basePotencia = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Introduce el exponente: ");
        int exponente = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine(Potencia(basePotencia, exponente));
    }
    static int Potencia(int basePotencia, int exponente)
    {
        int resultado;
        if (exponente == 0)
        {
            resultado = 1;
        }
        else
        {
            resultado = basePotencia * Potencia(basePotencia, exponente - 1);
        }
        return resultado;
    }
}