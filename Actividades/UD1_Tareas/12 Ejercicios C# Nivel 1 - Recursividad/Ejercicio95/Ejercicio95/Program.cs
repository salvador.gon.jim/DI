﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduzca un numero: ");
        int numero = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine(Fibonacci(numero));
    }
    static int Fibonacci(int numero)
    {
        int resultado;
        if (numero == 1 || numero == 2)
        {
            resultado = 1;
        }
        else
        {
            resultado = Fibonacci(numero - 1) + Fibonacci(numero - 2);
        }
            return resultado;
    }
}