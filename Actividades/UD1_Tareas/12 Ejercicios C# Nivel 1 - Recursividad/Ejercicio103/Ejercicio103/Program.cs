﻿internal class Program
{
    private static void Main(string[] args)
    {
        string nombreArchivo = "input.txt";
        int numCaracteresLinea = 79;
        StreamReader streamReader = new StreamReader(nombreArchivo);
        int linea = 0;

        while (streamReader.ReadLine != null)
        {
            string lineaActual = streamReader.ReadLine();
            if (lineaActual.Length > 79)
            {
                lineaActual = lineaActual.Substring(0, 79);
            }
            Console.WriteLine(lineaActual);
            Console.Write("Presione Intro para continuar...");
            linea++;

            Console.ReadLine();
        }
        streamReader.Close();
    }
}