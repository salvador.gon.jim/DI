﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduzca una cadena:");
        string cadena = Console.ReadLine();
        Console.WriteLine(InvertirCadena(cadena));
    }
    static string InvertirCadena(string cadena)
    {
        string cadenaInvertida;
        if (cadena.Length <= 1) 
        {
            cadenaInvertida = cadena;
        }
        else
        {
            cadenaInvertida = InvertirCadena(cadena.Substring(1)) + cadena[0];
        }
        return cadenaInvertida;
    }
}