﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un numero entero: ");
        int numero = Convert.ToInt32(Console.ReadLine());

        int numeroAlCuadrado  = Convert.ToInt32(Math.Pow(2, numero));

        Console.WriteLine(numeroAlCuadrado.ToString());
    }
}