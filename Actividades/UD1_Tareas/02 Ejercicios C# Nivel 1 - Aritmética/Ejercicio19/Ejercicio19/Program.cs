﻿internal class Program
{
    private static void Main(string[] args)
    { 
        Console.WriteLine("Introdice el primer numero: ");
        int numeroPrimero = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Introdice el segundo numero: ");
        int numeroSegundo = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Introdice el tercer numero: ");
        int numeroTercero = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Introdice el cuarto numero: ");
        int numeroCuarto = Convert.ToInt32(Console.ReadLine());
        int mediaAritmetica = (numeroPrimero + numeroSegundo + numeroTercero + numeroCuarto) / 4;
        Console.WriteLine(mediaAritmetica);
    }
}