﻿internal class Program
{
    private static void Main(string[] args)
    {
            Console.WriteLine("Introdice un numero: ");
            int x = Convert.ToInt32(Console.ReadLine());
            int resultado1 = -6 + x * 5;
            int resultado2 = (13 - 2) * x;
            int resultado3 = (x + -2) * (20 / 10);
            int resultado4 = (12 + x) / (5 - 4);

            Console.WriteLine(resultado1);
            Console.WriteLine(resultado2);
            Console.WriteLine(resultado3);
            Console.WriteLine(resultado4);
    }
}