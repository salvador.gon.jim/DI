﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introdice el primer numero: ");
        int numeroPrimero = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine("Introdice el segundo numero: ");
        int numeroSegundo = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine("Introdice el tercer numero: ");
        int numeroTercero = Convert.ToInt32(Console.ReadLine());

        int total = numeroPrimero * numeroSegundo * numeroTercero;

        Console.WriteLine(total);
    }
}