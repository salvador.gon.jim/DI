﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introdice el primer numero: ");
        int x = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine("Introdice el segundo numero: ");
        int y = Convert.ToInt32(Console.ReadLine());

        int total = x/y;
        int resto = x % y;

        Console.WriteLine(total.ToString());
        Console.WriteLine(resto.ToString());
    }
}