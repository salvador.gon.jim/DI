﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce el primer rango");
        int rangoMenor = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Introduce el segundo rango");
        int rangoMayor = Convert.ToInt32(Console.ReadLine());
        int y;

        for (int x = rangoMenor; x <= rangoMayor; x++)
        {
            y = x * x - 2 * x + 1;
            Console.Write(y + " ");
        }
    }
}