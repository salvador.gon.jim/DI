﻿internal class Program
{
    private static void Main(string[] args){
        int numeroPrimero;
        int numeroSegundo;
        int division;
        try {
            Console.WriteLine("Introdice el primer numero: ");
            numeroPrimero = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Introdice el segundo numero: ");
            numeroSegundo = Convert.ToInt32(Console.ReadLine());
            division = numeroPrimero / numeroSegundo;
            Console.WriteLine($"El resultado de dividir {numeroPrimero} / {numeroSegundo} es = {division}");
        } catch (DivideByZeroException) {
            Console.WriteLine("No se puede dividir por 0");
        }
    }
}