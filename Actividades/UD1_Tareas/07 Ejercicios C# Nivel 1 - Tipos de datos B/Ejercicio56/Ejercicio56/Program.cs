﻿internal class Program
{
    private static void Main(string[] args)
    {
        bool condicionSalida = false;
        while (!condicionSalida) {
            Console.WriteLine("Introduce un numero: ");
            int numero = Convert.ToInt32(Console.ReadLine());
            if (numero == 0 ) {
                condicionSalida = true;
            } else {
                String hexadecimal = Convert.ToString(numero, 16);
                String binario = Convert.ToString(numero, 2);
                Console.WriteLine(hexadecimal);
                Console.WriteLine(binario);
            }
        }
    }
}