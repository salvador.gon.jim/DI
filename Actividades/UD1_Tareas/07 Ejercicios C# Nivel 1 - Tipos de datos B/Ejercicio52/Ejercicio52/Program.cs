﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce una vocal: ");
        String vocal = Console.ReadLine().ToLower();
        switch (vocal)
        {
            case "a":
            case "e":
            case "i":
            case "o":
            case "u":
                Console.WriteLine("Es una vocal.");
                break;
            default:
                Console.WriteLine("No es una vocal");
                break;
        }
    }
}