﻿internal class Program
{
    private static void Main(string[] args)
    {
        String letraPrimera = "";
        int primerContador = 48;
        String letraSegunda = "";
        int segundoContador = 48;
        {
            for (int i = 0; i <= 16; i++) {
                for (int j = 0; j <= 16; j++) {
                    letraPrimera = Convert.ToChar(primerContador).ToString();
                    letraSegunda = Convert.ToChar(segundoContador).ToString();
                    Console.Write(letraPrimera + letraSegunda + " ");
                    segundoContador++;
                    if(segundoContador == 58) {
                        segundoContador = 97;
                    } else if (segundoContador == 123) {
                        segundoContador = 48;
                        primerContador++;
                        if(primerContador == 58) {
                            primerContador = 97;
                        } else if (primerContador == 123) {
                            primerContador = 48;
                        }
                    }
                }
                Console.WriteLine(" ");
            }
        }
    }
}