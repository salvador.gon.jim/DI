﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce el primer numero: ");
        int primerNumero = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Introduce el segundo numero: ");
        int segundoNumero = Convert.ToInt32(Console.ReadLine());
        bool sonPares = (primerNumero % 2 == 0 && segundoNumero % 2 == 0) ? true : false;
        Console.WriteLine(sonPares);
    }
}