﻿internal class Program
{

    private static void Main(string[] args) {
        Console.WriteLine("Introduce el nombre");
        string nombre = Console.ReadLine();
        Console.WriteLine("Introduce la edad");
        int edad = Convert.ToInt32(Console.ReadLine());
        Persona persona = new Persona { nombrePersona = nombre, edadPersona = edad };
        Console.WriteLine("!Hola " + persona.nombrePersona + "!");
    }
    struct Persona {
        public string nombrePersona;
        public int edadPersona;
    }
}