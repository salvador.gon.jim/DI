﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un numero: ");
        int numero = Convert.ToInt32(Console.ReadLine());
        int divisor = 2;
        bool primerFactorEncontrado = true;
        while (numero > 1) {
            while (numero % divisor == 0) {
                if (!primerFactorEncontrado) {
                    Console.Write("x");
                }
                Console.Write(divisor);
                numero = numero / divisor;
                primerFactorEncontrado = false;
            }
            divisor++;
        }
    }
}