﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce el primer numero: ");
        int primerNumero = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Introduce el segundo numero: ");
        int segundoNumero = Convert.ToInt32(Console.ReadLine());
        int resultado = 0;
        for(int i = 0; i < primerNumero; i++) {
            resultado += segundoNumero;
        }
        Console.WriteLine(primerNumero + "x" + segundoNumero + "= " + resultado);
    }
}