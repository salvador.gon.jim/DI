﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce el numero A: ");
        int A = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Introduce el numero B: ");
        int B = Convert.ToInt32(Console.ReadLine());
        int numPositivos = ((A > 0) && (B > 0)) ? 2 : ((A > 0) || (B > 0)) ? 1 : 0;
        Console.WriteLine(numPositivos);
    }
}