﻿class Program
{
    public static void Main(string[] args)
    {
        Console.WriteLine("Ingrese el total de dinero en caja: ");
        int caja = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Ingrese el valor de la compra: ");
        int compra = Convert.ToInt32(Console.ReadLine());
        if (compra > caja) {
            Console.WriteLine("No hay suficiente dinero en la caja para la compra.");
        } else {
            Console.WriteLine("Los billetes que quedan en caja son: ");
        }
        int cambio = caja - compra;
        while (cambio >= 100) {
            Console.Write("100 ");
            cambio -= 100;
        }
        while (cambio >= 50) {
            Console.Write("50 ");
            cambio -= 50;
        }
        while (cambio >= 20) {
            Console.Write("20 ");
            cambio -= 20;
        }
        while (cambio >= 10) {
            Console.Write("10 ");
            cambio -= 10;
        }
        while (cambio >= 5) {
            Console.Write("5 ");
            cambio -= 5;
        }
        while (cambio >= 2) {
            Console.Write("2 ");
            cambio -= 2;
        }
        while (cambio >= 1) {
            Console.Write("1 ");
            cambio -= 1;
        }
    }
}