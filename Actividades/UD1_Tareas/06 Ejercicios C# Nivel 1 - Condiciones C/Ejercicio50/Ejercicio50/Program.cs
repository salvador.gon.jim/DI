﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce el numero A: ");
        int A = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Introduce el numero B: ");
        int B = Convert.ToInt32(Console.ReadLine());
        string resutaldoA = (A > 0) ? "A es positivo" : "A es negativo";
        string resutaldoB = (B > 0) ? "B es positivo" : "B es negativo";
        string resultadoAmbos = (A > 0 && B > 0) ? "Ambos son positivos" : "A o B es negativo";
        Console.WriteLine(resutaldoA);
        Console.WriteLine(resutaldoB);
        Console.WriteLine(resultadoAmbos);
    }
}