﻿internal class Program
{
    static void Main(string[] args)
    {
        {
            Console.WriteLine("Introduce un numero: ");
            int numero = Convert.ToInt32(Console.ReadLine());
            int divisor = 2;
            while (numero % divisor != 0) {
                divisor++;
            }
            if (divisor == numero) {
                Console.WriteLine("Es primo");
            } else {
                Console.WriteLine("No es primo");
            }
        }
    }
}