﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un numero: ");
        int numero = Convert.ToInt32(Console.ReadLine());
        int valorAbsoluto = Math.Abs(numero);
        Console.WriteLine("El valor absoluto es " + valorAbsoluto);
    }
}