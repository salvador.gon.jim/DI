﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce una distancia en metros");
        float metros = Convert.ToInt32(Console.ReadLine());
        float kilometros = (float)metros/1000;
        float millas = (float)metros/1609;

        Console.WriteLine("Introduce el tiempo que tarda en recorrerlo");
        int horas = Convert.ToInt32(Console.ReadLine());
        int minutos = Convert.ToInt32(Console.ReadLine());
        int segundos = Convert.ToInt32(Console.ReadLine());

        float tiempoTotalSegundos = (horas*3600) + minutos*60 + segundos;
        float tiempoTotalHoras = tiempoTotalSegundos/3600;
        float metrosSegundo = metros/tiempoTotalSegundos;
        float kilometrosHora = kilometros/tiempoTotalHoras;
        float millasHora = millas/tiempoTotalHoras;

        Console.WriteLine("La velocidad en metros/seg es " + metrosSegundo);
        Console.WriteLine("La velocidad en kilometros/h es " + kilometrosHora);
        Console.WriteLine("La velocidad en millas/h es " + millasHora);
    }
}