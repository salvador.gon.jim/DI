﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce los grados Celcius");
        int celcius = Convert.ToInt32(Console.ReadLine());
        int kelvin = celcius + 273;
        int fahrenheit = celcius * 18 / 10 + 32;
        Console.WriteLine($"kelvin = {kelvin}");
        Console.WriteLine($"fahrenheit = {fahrenheit}");
    }
}