﻿internal class Program
{
    private static void Main(string[] args)
    {
        const double PI = Math.PI;
        Console.WriteLine("Introduce el radio de la esfera.");
        int radio = Convert.ToInt32(Console.ReadLine());
        double superficie = 4 * PI * Math.Pow(radio, 2);
        double volumen = 4/3 * PI * Math.Pow(radio, 3);
        Console.WriteLine("Superfice " +  superficie);
        Console.WriteLine("Volumen " + volumen);
    }
}