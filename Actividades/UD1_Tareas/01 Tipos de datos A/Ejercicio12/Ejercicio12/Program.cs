﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un numero:");
        int numero = Convert.ToInt32(Console.ReadLine());
        for (int i = 0; i < 11; i++)
        {
            int resultado = numero * i;
            Console.WriteLine($"{numero}x{i}= {resultado}");
        }
    }
}