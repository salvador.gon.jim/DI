﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce el primer numero:");
        int primerNumero = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Introduce el segundo numero:");
        int segundoNumero = Convert.ToInt32(Console.ReadLine());

        int suma = primerNumero + segundoNumero;
        int resta = primerNumero - segundoNumero;
        int multiplicacion = primerNumero * segundoNumero;
        int division = primerNumero / segundoNumero;
        int modulo = primerNumero % segundoNumero;

        Console.WriteLine($"{primerNumero} + {segundoNumero} = {suma}");
        Console.WriteLine($"{primerNumero} - {segundoNumero} = {resta}");
        Console.WriteLine($"{primerNumero} x {segundoNumero} = {multiplicacion}");
        Console.WriteLine($"{primerNumero} / {segundoNumero} = {division}");
        Console.WriteLine($"{primerNumero} mod {segundoNumero} = {modulo}");
    }
}