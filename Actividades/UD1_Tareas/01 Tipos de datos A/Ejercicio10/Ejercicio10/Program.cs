﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce el ancho:");
        double ancho = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("Introduce la altura:");
        double alto = Convert.ToDouble(Console.ReadLine());
        double perimetro = ancho + ancho + alto + alto;
        double area = ancho * alto;
        double diagonal = Math.Pow(ancho, 2) + Math.Pow(alto, 2);
        diagonal = Math.Sqrt(diagonal);
        Console.WriteLine($"Perimetro: {perimetro}");
        Console.WriteLine($"Area: {area}");
        Console.WriteLine($"Diagonal: {diagonal}");
    }
}