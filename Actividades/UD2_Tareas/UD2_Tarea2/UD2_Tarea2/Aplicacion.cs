﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UD2_Tarea2
{
    internal class Aplicacion
    {
        private UtilES utilES;
        private Empleado empleado;
        private Empleados empleados;
        public Aplicacion()
        {
            utilES = new UtilES();
            empleados = new Empleados();
        }

        public void ejecutar()
        {
            mostrarMenu();
        }

        private void mostrarMenu()
        {
            int opcion;
            do
            {
                Console.WriteLine("0 - Salir");
                Console.WriteLine("1 - Crear empleado");
                Console.WriteLine("2 - Actualizar salario");
                Console.WriteLine("3 – Mostrar nombre");
                Console.WriteLine("4 - Mostrar edad");
                Console.WriteLine("5 - Mostrar empleado");
                Console.WriteLine("6 - Mostrar empleados");
                Console.WriteLine("7 – Borrar empleado");
                Console.WriteLine("Introduzca su opción: ");
                opcion = utilES.pideEntero("");
                switch (opcion)
                {
                    case 0:
                        break;
                    case 1:
                        crearEmpeado();
                        esperar();
                        break;
                    case 2:
                        acutualizarSalario();
                        esperar();
                        break;
                    case 3:
                        mostrarNombre();
                        esperar();
                        break;
                    case 4:
                        mostrarEdad();
                        esperar();
                        break;
                    case 5:
                        mostrarEmpleado();
                        esperar();
                        break;
                    case 6:
                        mostrarEmpleados();
                        esperar();
                        break;
                    case 7:
                        borrarEmpleado();
                        esperar();
                        break;
                }
            } while (opcion != 0);
        }

        private void crearEmpeado() 
        {    
            string nombre = utilES.pideString("Introduce el nombre:");
            string apellido1 = utilES.pideString("Introduce el primer apellido:");   
            string apellido2 = utilES.pideString("Introduce el segundo apellido:");     
            int edad = utilES.pideEntero("Introduce la edad:"); 
            string nif = utilES.pideDNI("Introduce el NIF:");
            double salario = utilES.pideDoble("Introduce el salario:");
            empleados.nuevoEmpleado(nombre, apellido1, apellido2,edad,nif,salario);
        }
        private void acutualizarSalario()
        {
            string nif = utilES.pideString("Introduce el NIF:");
            double salario = utilES.pideDoble("Introduce el salario.");
            empleados.modificarSalarioEmpleado(nif, salario);
        }

        private void mostrarNombre()
        {
            string nif = utilES.pideString("Introduce el NIF:");
            empleados.mostarNombreEmpleado(nif);
        }

        private void mostrarEdad()
        {
            string nif = utilES.pideString("Introduce el NIF:");
            empleados.mostrarEdadEmpleado(nif);
        }

        private void mostrarEmpleado()
        {
            string nif = utilES.pideString("Introduce el NIF:");
            empleados.mostrarEmpleado(nif);
        }

        private void mostrarEmpleados()
        {
            empleados.mostrarTodosEmpleados();
        }

        private void borrarEmpleado()
        {
            string nif = utilES.pideString("Introduce el NIF:");
            empleados.borrarEmpleado(nif);
        }
        private void esperar()
        {
            Console.WriteLine("Pulsa INTRO para continuar...");
            Console.ReadLine();
            Console.Clear();
        }
    }
}