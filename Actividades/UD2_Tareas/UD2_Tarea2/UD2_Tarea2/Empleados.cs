﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UD2_Tarea2
{
    internal class Empleados
    {
        private List<Empleado> listaEmpleados;
        public Empleados()
        {
            listaEmpleados = new List<Empleado>();
        }

        public Empleado obtenerEmpleado(string nif)
        {
            Empleado empleado = null;
            for (int i = 0; i < listaEmpleados.Count; i++)
            {
                if (listaEmpleados[i].getNif() == nif)
                {
                    empleado = listaEmpleados[i];
                }
            }
            return empleado;
        }
        
        public void nuevoEmpleado(string nombre, string apellido1, string apellido2, int edad, string nif, double salario)
        {
            Empleado empleado = obtenerEmpleado(nif);
            if(empleado == null)
            {
                empleado = new Empleado(nombre, apellido1, apellido2, edad, nif, salario);
                listaEmpleados.Add(empleado);
            }
            else
            {
                Console.WriteLine("Ya existe un empleado con ese nif");
            }
        }

        public void modificarSalarioEmpleado(string nif, double salario)
        {
            Empleado empleado = obtenerEmpleado(nif);
            if(empleado != null)
            {
                empleado.setSalario(salario);
                Console.WriteLine("El salario del empleado " + nif + " ha sido modificado a: " + salario);
            } else
            {
                Console.WriteLine("El empleado con ese nif no existe.");
            }
        }

        public void mostarNombreEmpleado(string nif)
        {
            Empleado empleado = obtenerEmpleado(nif);
            if (empleado != null)
            {
                string nombreEmpleado = empleado.getNombre();
                Console.WriteLine(nombreEmpleado);
            }
            else
            {
                Console.WriteLine("El empleado con ese nif no existe.");
            }
        }

        public void mostrarEdadEmpleado(string nif)
        {
            Empleado empleado = obtenerEmpleado(nif);
            if (empleado != null)
            {
                int edadEmpleado = empleado.getEdad();
                Console.WriteLine(edadEmpleado);
            }
            else
            {
                Console.WriteLine("El empleado con ese nif no existe.");
            }
        }

        public void mostrarEmpleado(string nif)
        {
            Empleado empleado = obtenerEmpleado(nif);
            if (empleado != null)
            {
                Console.WriteLine(empleado);
            }
            else
            {
                Console.WriteLine("El empleado con ese nif no existe.");
            }
        }

        public void mostrarTodosEmpleados()
        {
            if (listaEmpleados.Count != 0)
            {
                for (int i = 0; i < listaEmpleados.Count; i++)
                {
                    Console.WriteLine(listaEmpleados[i]);
                }
            }
            else
            {
                Console.WriteLine("No hay empleados registrados.");
            }

        }

        public void borrarEmpleado(string nif)
        {
            Empleado empleado = obtenerEmpleado(nif);
            if (empleado != null)
            {
                listaEmpleados.Remove(empleado);
                Console.WriteLine("El empleado se ha borrado.");
            }
            else
            {
                Console.WriteLine("El empleado con ese nif no existe.");
            }
        }
    }
}
