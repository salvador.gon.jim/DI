﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UD2_Tarea2
{
    internal class Empleado
    {
        private string nombre;
        private string apellido1;
        private string apellido2;
        private int edad;
        private string nif;
        private double salario;
        public Empleado(string nombre, string apellido1, string apellido2, int edad, string nif, double salario)
        {
            this.nombre = nombre;
            this.apellido1 = apellido1;
            this.apellido2 = apellido2;
            this.edad = edad;
            this.nif = nif;
            this.salario = salario;
        }

        public string getNombre()
        {
            return nombre;
        }

        public string getApellido1()
        {
            return apellido1;
        }

        public string getApellido2()
        {
            return apellido2;
        }

        public int getEdad()
        {
            return edad;
        }

        public string getNif()
        {
            return nif;
        }

        public double getSalario()
        {
            return salario;
        }

        public void setSalario(double salario)
        {
            this.salario = salario;
        }

        public void setNif(string nif)
        {
            this.nif = nif;
        }

        public override string ToString()
        {
            return "Nombre: " + nombre + " Apellido1: " + apellido1 + " Apellido2: " + apellido2 + " Edad: " + edad + " NIF: " + nif + " Salario: " + salario;
        }
    }
}
