﻿using UD2_Tarea2;

internal class Program
{
    private static void Main(string[] args)
    {
        try
        {
            Aplicacion aplicaicon = new Aplicacion();
            aplicaicon.ejecutar();
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error inesperado.");
        }
    }
}