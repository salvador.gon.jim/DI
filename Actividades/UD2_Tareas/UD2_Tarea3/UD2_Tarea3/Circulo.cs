﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UD2_Tarea3
{

    /**
    * Clase Circulo que hereda de Figura
    */
    internal class Circulo : Figura
    {
        private float radio;
        public Circulo(float radio) 
        {
            this.radio = radio;
        }

        /**
        * Calcula el perimetro de un circulo
        *
        * @return el resultado del calculo del perimetro
        */
        public override float Perimetro()
        {
            return 2 * (float)Math.PI * radio;
        }

        /**
        * Calcula el area de un circulo
        *
        * @return el resultado del calculo del area
        */
        public override float Area()
        {
            return (float)Math.PI * radio * radio;
        }
    }
}
