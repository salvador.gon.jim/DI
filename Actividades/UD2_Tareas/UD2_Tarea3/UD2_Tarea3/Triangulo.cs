﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UD2_Tarea3
{

    /**
    * Clase Triangulo que hereda de Figura
    */
    internal class Triangulo : Figura
    {
        private float baseTriangulo;
        private float alturaTriangulo;
        public Triangulo(float baseTriangulo, float alturaTriangulo)
        {
            this.baseTriangulo = baseTriangulo;
            this.alturaTriangulo = alturaTriangulo;
        }

        /**
        * Calcula el perimetro de un triangulo rectangulo
        *
        * @return el resultado del calculo del perimetro
        */
        public override float Perimetro()
        {
            return alturaTriangulo + baseTriangulo + (float)Math.Sqrt((alturaTriangulo*alturaTriangulo)+(baseTriangulo*baseTriangulo));
        }

        /**
        * Calcula el area de un triangulo rectangulo
        *
        * @return el resultado del calculo del area
        */
        public override float Area()
        {
            return (baseTriangulo * alturaTriangulo) / 2;
        }
    }
}
