﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UD2_Tarea3
{

    /**
    * Clase padre de la que heredan las figuras
    */
    internal class Figura
    {

        /**
        * Calcula el perimetro de una figura
        *
        * @return el resultado del calculo del perimetro
        */
        public virtual float Perimetro()
        {
            return 0;
        }

        /**
        * Calcula el area de una figura
        *
        * @return el resultado del calculo del area
        */
        public virtual float Area()
        {
            return 0;
        }
    }
}
