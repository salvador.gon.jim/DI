﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UD2_Tarea3
{

    /**
    * Clase que contiene los métodos para la entrada y salida de datos por teclado
    */
    internal class UtilES
    {

        /**
         * Muestra un mensaje por pantalla y salta de linea
         *
         * @param cadena mensaje a mostrar
         */
        public void mensaje(string texto)
        {
            try
            {
                Console.WriteLine(texto);
            }
            catch (IOException ioe)
            {
                Console.WriteLine("Ha ocurrido un error de Entrada/Salida.");
            }
        }

        /**
        * Pide una cadena por teclado
        *
        * @param mensaje mensaje a mostrar
        * @return cadena introducida
        */
        public string pideString(string texto)
        {
            string cadena = "";
            bool correcto = false;
            do
            {
                mensaje(texto);
                try
                {
                    cadena = Console.ReadLine();
                    cadena = cadena.Trim();
                    if (cadena.Length > 0)
                    {
                        correcto = true;
                    }
                    else
                    {
                        Console.WriteLine("No has introducido ningún valor. Por favor, vuelva a intentarlo.");
                    }
                }
                catch (IOException ioe)
                {
                    Console.WriteLine("Error en la entrada de datos");
                }
            }
            while (!correcto);
            return cadena;
        }

        /**
        * Pide un int por teclado
        *
        * @param mensaje mensaje a mostrar
        * @return float
        */
        public int pideEntero(string mensaje)
        {
            int numero = 0;
            string cadena = "";
            bool correcto = false;
            do
            {
                cadena = pideString(mensaje);
                try
                {
                    numero = int.Parse(cadena);
                    correcto = true;
                }
                catch (FormatException nfe)
                {
                    Console.WriteLine("El valor introducido no es un número entero. Por favor, vuelva a intentarlo.");
                }
            }
            while (!correcto);
            return numero;
        }

        /**
        * Pide un float por teclado
        *
        * @param mensaje mensaje a mostrar
        * @return float
        */
        public float pideFloat(string mensaje)
        {
            float numero = 0;
            string cadena = "";
            bool correcto = false;
            do
            {
                cadena = pideString(mensaje);
                try
                {
                    numero = float.Parse(cadena);
                    correcto = true;
                }
                catch (FormatException nfe)
                {
                    Console.WriteLine("El valor introducido no es un número entero. Por favor, vuelva a intentarlo.");
                }
            }
            while (!correcto);
            return numero;
        }
    }
}
