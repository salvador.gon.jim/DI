﻿using UD2_Tarea3;
using static System.Net.Mime.MediaTypeNames;

internal class Program
{
    private static void Main(string[] args)
    {
        try
        {
            Aplicacion aplicaicon = new Aplicacion();
            aplicaicon.ejecutar();
        }
        catch (Exception e)
        {
            Console.WriteLine("Error inesperado.");
        }
    }
}