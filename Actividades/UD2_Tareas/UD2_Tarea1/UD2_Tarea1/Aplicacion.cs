﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UD2_Tarea1
{
    internal class Aplicacion
    {
        private UtilES utilES;
        private Empleado empleado;
        public Aplicacion() { 
            utilES = new UtilES();
        }

        public void ejecutar()
        {
            mostrarMenu();
        }

        private void mostrarMenu()
        {
            int opcion;
            do
            {
                Console.WriteLine("0 - Salir");
                Console.WriteLine("1 - Crear empleado");
                Console.WriteLine("2 - Actualizar salario");
                Console.WriteLine("3 – Mostrar nombre");
                Console.WriteLine("4 - Mostrar NIF");
                Console.WriteLine("5 - Actualizar NIF");
                Console.WriteLine("6 - Mostrar edad");
                Console.WriteLine("7 – Mostrar empleado");
                Console.WriteLine("Introduzca su opción: ");
                opcion = utilES.pideEntero("");
                switch (opcion)
                {
                    case 0:
                        break;
                    case 1:
                        crearEmpeado();
                        break;
                    case 2:
                        actualizarSalario(empleado);
                        break;
                    case 3:
                        mostrarNombre(empleado);
                        break;
                    case 4:
                        mostrarNIF(empleado);
                        break;
                    case 5:
                        actualizarNIF(empleado);
                        break;
                    case 6:
                        mostrarEdad(empleado);
                        break;
                    case 7:
                        mostrarEmpleado(empleado);
                        break;
                }
            } while (opcion != 0);
        }


        private Empleado crearEmpeado()
        {
            if (empleado == null)
            {
                string nombre = utilES.pideString("Introduce el nombre:");
                string apellido1 = utilES.pideString("Introduce el primer apellido:");
                string apellido2 = utilES.pideString("Introduce el segundo apellido:");
                int edad = utilES.pideEntero("Introduce la edad:");
                string nif = utilES.pideString("Introduce el NIF:");
                double salario = utilES.pideDoble("Introduce el salario:");
                empleado = new Empleado(nombre, apellido1, apellido2, edad, nif, salario);
            }
            else
            {
                Console.WriteLine("El empleado ya existe");
            }
            return empleado;
        }


        private void actualizarSalario(Empleado empleado)
        {
            if (empleado == null)
            {
                utilES.mensaje("El empleado no existe.");
            }
            else
            {
                double salario = utilES.pideDoble("Introduce el nuevo salario.");
                empleado.setSalario(salario);
            }
        }
        private void mostrarNombre(Empleado empleado)
        {
            if (empleado == null)
            {
                utilES.mensaje("El empleado no existe.");
            }
            else
            {
                utilES.mensaje(empleado.getNombre());
            }
        }
        private void mostrarNIF(Empleado empleado)
        {
            if (empleado == null)
            {
                utilES.mensaje("El empleado no existe.");
            }
            else
            {
                utilES.mensaje(empleado.getNif());
            }
        }
        private void actualizarNIF(Empleado empleado)
        {
            if (empleado == null)
            {
                utilES.mensaje("El empleado no existe.");
            }
            else
            {
                string nif = utilES.pideString("Introduce el nuevo NIF.");
                empleado.setNif(nif);
            }
        }
        private void mostrarEdad(Empleado empleado)
        {
            if (empleado == null)
            {
                utilES.mensaje("El empleado no existe.");
            }
            else
            {
                utilES.mensaje("" + empleado.getEdad());
            }
        }
        private void mostrarEmpleado(Empleado empleado)
        {
            if (empleado == null)
            {
                utilES.mensaje("El empleado no existe.");
            }
            else
            {
                utilES.mensaje(empleado.ToString());
            }
        }
    }
    
}
