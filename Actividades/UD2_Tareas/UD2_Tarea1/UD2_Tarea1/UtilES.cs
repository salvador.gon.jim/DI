﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UD2_Tarea1
{
    internal class UtilES
    {
        public void mensaje(string texto)
        {
            try { 
                Console.WriteLine(texto);
            } catch (IOException ioe) {
                Console.WriteLine("Ha ocurrido un error de Entrada/Salida.");
            }
        }

        public string pideString(string texto)
        {
            string cadena = "";
            bool correcto = false;
            do
            {
                mensaje(texto);
                try
                {
                    cadena = Console.ReadLine();
                    cadena = cadena.Trim();
                    if (cadena.Length > 0)
                    {
                        correcto = true;
                    }
                    else
                    {
                        Console.WriteLine("No has introducido ningún valor. Por favor, vuelva a intentarlo.");
                    }
                }
                catch (IOException ioe)
                {
                    Console.WriteLine("Error en la entrada de datos");
                }
            }
            while (!correcto);
            return cadena;
        }


        public int pideEntero(string mensaje)
        {
            int numero = 0;
            string cadena = "";
            bool correcto = false;
            do
            {
                cadena = pideString(mensaje);
                try
                {
                    numero = int.Parse(cadena);
                    correcto = true;
                }
                catch (FormatException nfe)
                {
                    Console.WriteLine("El valor introducido no es un número entero. Por favor, vuelva a intentarlo.");
                }
            }
            while (!correcto);
            return numero;
        }


        public double pideDoble(string mensaje)
        {
            double numero = 0;
            string cadena = "";
            bool correcto = false;
            do
            {
                cadena = pideString(mensaje);
                try
                {
                    numero = double.Parse(cadena);
                    correcto = true;
                }
                catch (FormatException nfe)
                {
                    Console.WriteLine("El valor introducido no es un número entero. Por favor, vuelva a intentarlo.");
                }
            }
            while (!correcto);
            return numero;
        }

        static bool ValidarDNI(string dni)
        {
            if (dni.Length != 9)
            {
                return false;
            }

            string numeros = dni.Substring(0, 8);
            char letra = char.ToUpper(dni[8]);

            if (!int.TryParse(numeros, out int numero))
            {
                return false;
            }

            char letraCalculada = "TRWAGMYFPDXBNJZSQVHLCKE"[numero % 23];

            return letra == letraCalculada;
        }
    }
}
