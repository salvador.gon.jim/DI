﻿using UD2_Tarea1;

internal class Tarea1
{
    private static void Main(string[] args)
    {
        try
        {
            Aplicacion aplicaicon = new Aplicacion();
            aplicaicon.ejecutar();
        } catch (Exception ex)
        {
            Console.WriteLine("Error inesperado.");
        }
    }
}